/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package volvis;

/**
 *
 * @author michel
 */
public class TFColor {
    public double r, g, b, a;

    public TFColor() {
        r = g = b = a = 1.0;
    }
    
    public TFColor(double red, double green, double blue, double alpha) {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }
    
    @Override
    public String toString() {
        String text = "(" + r + ", " + g + ", " + b + ", " + a + ")";
        return text;
    }
    
    /**
     * @author Bill
     * 
     * Fractional to integral method. To avoid repeated code.
     * 
     * @return int
     */
    public int toARGB() {
        int int_a = a <= 1.0 ? (int) Math.floor(a * 255) : 255;
        int int_r = r <= 1.0 ? (int) Math.floor(r * 255) : 255;
        int int_g = g <= 1.0 ? (int) Math.floor(g * 255) : 255;
        int int_b = b <= 1.0 ? (int) Math.floor(b * 255) : 255;
        int ans = (int_a << 24) | (int_r << 16) | (int_g << 8) | int_b;
        return ans;
    }
}
