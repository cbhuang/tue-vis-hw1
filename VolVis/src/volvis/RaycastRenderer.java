/**
 * TODO: compare MIP with COMP with the same orange color
 */
package volvis;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import gui.RaycastRendererPanel;
import gui.TransferFunction2DEditor;
import gui.TransferFunctionEditor;
import java.awt.image.BufferedImage;
import util.TFChangeListener;
import util.VectorMath;
import volume.VoxelGradient;  // For 2D Transfer Function
import volume.GradientVolume;
import volume.Volume;

/**
 *
 * @author michel
 */
public class RaycastRenderer extends Renderer implements TFChangeListener {

    private Volume volume = null;
    private GradientVolume gradients = null;
    RaycastRendererPanel panel;
    TransferFunction tFunc;
    TransferFunctionEditor tfEditor;
    TransferFunction2DEditor tfEditor2D;
    /** custom variables */
    public int renderingMode = 0;  // slicer,mip,comp,tf2d
    public boolean shading = false;  // apply shading in tf2d
    public int resolution;  // speed up procession
    
    public RaycastRenderer() {
        panel = new RaycastRendererPanel(this);
        panel.setSpeedLabel("0");
        resolution = panel.getResolution();
    }

    public void setVolume(Volume vol) {
        System.out.println("Assigning volume");
        volume = vol;

        System.out.println("Computing gradients");
        gradients = new GradientVolume(vol);

        // set up image for storing the resulting rendering
        // the image width and height are equal to the length of the volume diagonal
        int imageSize = (int) Math.floor(Math.sqrt(vol.getDimX() * vol.getDimX() + vol.getDimY() * vol.getDimY() + vol.getDimZ() * vol.getDimZ()));
        if (imageSize % 2 != 0) {
            imageSize = imageSize + 1;
        }
        image = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_ARGB);
        // create a standard TF where lowest intensity maps to black, the highest to white, and opacity increases
        // linearly from 0.0 to 1.0 over the intensity range
        tFunc = new TransferFunction(volume.getMinimum(), volume.getMaximum());
        
        // uncomment this to initialize the TF with good starting values for the orange dataset 
        tFunc.setTestFunc();

        tFunc.addTFChangeListener(this);
        tfEditor = new TransferFunctionEditor(tFunc, volume.getHistogram());
        
        tfEditor2D = new TransferFunction2DEditor(volume, gradients);
        tfEditor2D.addTFChangeListener(this);

        System.out.println("Finished initialization of RaycastRenderer");
    }
    
    /**
     * Release memory before loading new volume
     */
    public void clearVolume() {
        volume = null;
        gradients = null;
        System.out.println("Finished clearing data of RaycastRenderer");
    }

    public RaycastRendererPanel getPanel() {
        return panel;
    }

    public TransferFunction2DEditor getTF2DPanel() {
        return tfEditor2D;
    }
    
    public TransferFunctionEditor getTFPanel() {
        return tfEditor;
    }
     
    
    short getVoxel(double[] coord) {

        if (coord[0] < 0 || coord[0] >= volume.getDimX() || coord[1] < 0 || coord[1] >= volume.getDimY()
                || coord[2] < 0 || coord[2] >= volume.getDimZ()) {
            return 0;
        }
        
        int x0 = (int) Math.floor(coord[0]);
        int y0 = (int) Math.floor(coord[1]);
        int z0 = (int) Math.floor(coord[2]);
        
        // speed up when dragging
        if(interactiveMode) {
            return volume.getVoxel(x0, y0, z0);
        } else {
            /**
              * Q1-1. Trilinear Interpolation (Huilin)
              */
            // create the conner coordinates of the voxel originating at (x,y,z), extending to the diagnal of (x1,y1,z1)
            int x1 = (int) x0 + 1;
            int y1 = (int) y0 + 1;
            int z1 = (int) z0 + 1;
            // alpha, beta, gamma
            double alpha = coord[0] - x0;
            double beta = coord[1] - y0;
            double gamma = coord[2] - z0;
            // find the intensity value, sx_i for each vortex of the cubic in slide#2 page 7
            double sx0 = volume.getVoxel(x0, y0, z0);
            double sx1 = volume.getVoxel(x1, y0, z0);
            double sx2 = volume.getVoxel(x0, y1, z0);
            double sx3 = volume.getVoxel(x1, y1, z0);
            double sx4 = volume.getVoxel(x0, y0, z1);
            double sx5 = volume.getVoxel(x1, y0, z1);
            double sx6 = volume.getVoxel(x0, y1, z1);
            double sx7 = volume.getVoxel(x1, y1, z1);
            //find the intensity value sx of the interpolated point (x,y,z)
            double sx = (1-alpha)*(1-beta)*(1-gamma)*sx0                    
                        + alpha*(1-beta)*(1-gamma)*sx1
                        + (1-alpha)*beta*(1-gamma)*sx2
                        + alpha*beta*(1-gamma)*sx3
                        + (1-alpha)*(1-beta)*gamma*sx4
                        + alpha*(1-beta)*gamma*sx5
                        + (1-alpha)*beta*gamma*sx6 
                        + alpha*beta*gamma*sx7;
            return (short) sx;
        }
    }

    
    void slicer(double[] viewMatrix) {
        
        // clear image to black and transparent
        clearImage();  // Bill: refactored for simplicity

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double maxIntensity = volume.getMaximum();
        TFColor voxelColor = new TFColor();  // r = g = b = a = 1.0

        for (int j = 0; j < image.getHeight(); j += resolution) {
            for (int i = 0; i < image.getWidth(); i += resolution) {
                
                // plane formula (no accumulation over a ray)
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter) + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter) + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter) + volumeCenter[2];
                int val = getVoxel(pixelCoord);
                
                // Map the intensity to a grey value by linear scaling
                voxelColor.r = val / maxIntensity;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val > 0 ? 1.0 : 0.0;  // this makes intensity 0 completely transparent and the rest opaque
                
                // Alternatively, apply the transfer function to obtain a color
                // voxelColor = tFunc.getColor(val);
                
                // BufferedImage expects a pixel color packed as ARGB in an int
                if (resolution > 1) {  // lower resolution
                    setImageARGBwithResolution(i, j, voxelColor);
                } else {  // normal case
                    image.setRGB(i, j, voxelColor.toARGB());  // Bill: simplified by adding a class method
                }
            }  // i
        }  // j
    }  // slicer()
    
    
    /**
     * Q1-2. (1) Maximum Intensity Projection (Huilin)
     */
    void mip(double[] viewMatrix) {

        // clear image to black and transparent
        clearImage();

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double maxIntensity = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        
        for (int j = 0; j < image.getHeight(); j += resolution) {
            for (int i = 0; i < image.getWidth(); i += resolution) {
                
                // Huilin: accumulate maximum intensity over the 3rd dimension
                int maxVal=0;
                for (int k = 0; k < volume.getDimX(); k += resolution){
                    // Bill: plane formula plus the 3rd dimension of the viewing ray: (viewVec[0] * k)
                    pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter) + viewVec[0] * k + volumeCenter[0];
                    pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter) + viewVec[1] * k + volumeCenter[1];
                    pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter) + viewVec[2] * k + volumeCenter[2];
                    int val = getVoxel(pixelCoord);
                    // update when greater
                    if (val > maxVal) {
                        maxVal = val;
                    }
                }
                
                // Map the intensity to a grey value by linear scaling
                voxelColor.r = maxVal / maxIntensity;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = maxVal > 0 ? 1.0 : 0.0;  // this makes intensity 0 completely transparent and the rest opaque

                // Alternatively, apply the transfer function to obtain a color
                // voxelColor = tFunc.getColor(val);                
                
                // set BufferedImage
                if (resolution > 1) {  // lower resolution
                    setImageARGBwithResolution(i, j, voxelColor);
                } else {  // normal case
                    image.setRGB(i, j, voxelColor.toARGB());
                }
            }  // i
        }  // j
    }  // mip()

    
    /**
     * Q1-2. (2) Compositing View (Huilin)
     */
    void compositing(double[] viewMatrix) {

        // clear image to black and transparent
        clearImage();

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        TFColor voxelColor = new TFColor();

        /**
         * Huilin: for each pixel in our image, we glide this image across the 
         * X axis of the 3D object -- for each point sampled in the 3D object, 
         * we call this point "pixelCoord"
         */
        for (int j = 0; j < image.getHeight(); j += resolution) {
            for (int i = 0; i < image.getWidth(); i += resolution) {
                
                /** 
                 * Bill: Initialize the compositing pixel being accumulated to
                 *  "transparent black" (the null background we want when nothing is there)
                 */
                TFColor compColor = new TFColor(0, 0, 0, 0);
                
                // accumulate through the 3rd dimension (the ray)
                for (int k = 0; k < volume.getDimX(); k += resolution){
                    
                    // get the intensity of the point "pixelCoord" in 3D object
                    pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter) + viewVec[0] * k + volumeCenter[0];
                    pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter) + viewVec[1] * k + volumeCenter[1];
                    pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter) + viewVec[2] * k + volumeCenter[2];
                    int val = getVoxel(pixelCoord);

                    /** 
                     * Huilin & Bill: The compositing fuction from UI (TransferFunctionEditor):
                     * From intensity value, use the Lookuptable to find the corresponding opacity 
                     * and color value of this pixelCoord, and store it in voxelColor
                     */
                    voxelColor = tFunc.getColor(val);

                    /** 
                     * Huilin: implement compositing function to get the final opacity and RGB 
                     * for all of the sample points.
                     * As we glide our pixel along the X axis, we accumulate the sample voxel 
                     * colors by adding new sample colors to the sample colors we have already 
                     * added. Therefore compColor is the final color of the pixel in our 
                     * computer screen image up to a certain sample point
                     */
                    /**
                     * Bill: slide2 p.31, recursive formula at the bottom
                     */
                    compColor.r = voxelColor.a * voxelColor.r + (1 - voxelColor.a) * compColor.r;
                    compColor.g = voxelColor.a * voxelColor.g + (1 - voxelColor.a) * compColor.g;
                    compColor.b = voxelColor.a * voxelColor.b + (1 - voxelColor.a) * compColor.b;
                    /**
                     * Bill: Levoy (1988) p.32 top-right, manually converted to the recursive form
                     * a_N^{total} = a_N + (1-a_N) * a_{N-1}^{total}
                     */
                    compColor.a = voxelColor.a + (1 - voxelColor.a) * compColor.a;
                }  // k
                
                // set BufferedImage
                if (resolution > 1) {  // lower resolution
                    setImageARGBwithResolution(i, j, compColor);
                } else {  // normal case
                    image.setRGB(i, j, compColor.toARGB());
                }
            }  // i
        }  // j
    }  // compositing()
    
    
    /**
     * Q2-1. 2D Transfer Function & Phong Shading (Bill)
     */
    void transfer2d(double[] viewMatrix) {

        // clear image to black and transparent
        clearImage();

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // set volumeCenter
        int imageCenter = image.getWidth() / 2;  // image is square
        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        
        // loop variables
        TFColor finalColor;  // The answer we want (accumulated over the ray)
        TFColor voxelColor = new TFColor();  // color of local pixel
        int intensity;  // intensity value of local pixel
        VoxelGradient voxGrad;  // local gradient
        
        // User defined values from the UI
        TFColor uiColor = tfEditor2D.triangleWidget.color;
        short uiIntensity = tfEditor2D.triangleWidget.baseIntensity;
        double uiRadius = tfEditor2D.triangleWidget.radius;
        
        /** Shading variables */
        // light color (r,g,b)(arbitrarily chosen)
        double[] lightIntensity = {uiColor.r, uiColor.g, uiColor.b};  // choose the same color with uiColor
        //double[] lightIntensity = {1.0, 1.0, 1.0}; // choose white light
        
        // light direction unit vector (x,y,z)(arbitrarily chosen)
        // this vector points from the observer to the parallel light source
        double[] lightVec = {1.0, 0.0, 0.0};  // The light comes from the right side -- should see brighter at the right side
        
        // coefficients given in the assignment
        double k_ambient = 0.1;
        double k_diff = 0.7;
        double k_spec = 0.2;
        double alpha = 10.0;
        // temp variables
        double[] normVec = new double[3];  // normal vector
        double N_dot_L;
        double V_dot_R;
        // end shading variables
        
        // sample on a plane through the origin of the volume data
        for (int j = 0; j < image.getHeight(); j += resolution) {
            for (int i = 0; i < image.getWidth(); i += resolution) {
                
                // Bill: Initialize the accumulated color to "opaque black"
                finalColor = new TFColor(0.0, 0.0, 0.0, 1.0);
                
                // accumulate through the 3rd dimension (the ray)
                for (int k = 0; k < volume.getDimX(); k += resolution) {
                    
                    // get the intensity of the point "pixelCoord" in 3D object
                    pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter) + viewVec[0] * k + volumeCenter[0];
                    pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter) + viewVec[1] * k + volumeCenter[1];
                    pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter) + viewVec[2] * k + volumeCenter[2];
                    intensity = getVoxel(pixelCoord);  

                    // process only when val > 0 (something is being scanned)
                    if (intensity > 0) {
                        
                        // getting the gradient of the point pixelCoord in x, y, z directions
                        voxGrad = gradients.getGradient((int)Math.floor(pixelCoord[0]), (int)Math.floor(pixelCoord[1]), (int)Math.floor(pixelCoord[2])); 

                        // set the RGB according to the user defined RGB in the triangle widget
                        voxelColor.r = uiColor.r;
                        voxelColor.g = uiColor.g;
                        voxelColor.b = uiColor.b;

                        // Implementation of Levoy (1988) Eqn.3
                        if (voxGrad.mag == 0 && intensity == uiIntensity) {
                            voxelColor.a = uiColor.a;  // Bill: yes this is correct. It is (alpha_v*1.0) on the paper (confirmed with TA)
                        }
                        else if (voxGrad.mag > 0 && 
                                 Math.abs(intensity - uiIntensity) <= uiRadius * voxGrad.mag) {
                            voxelColor.a = uiColor.a * (1 - Math.abs(uiIntensity - intensity) / voxGrad.mag / uiRadius);
                        }
                        else {
                            voxelColor.a = 0.0;
                        }
                        
                        /**
                         * Q2-2. Phong shading (Bill)
                         * 
                         * Formula: slide3 p.16
                         * References: https://en.wikipedia.org/wiki/Phong_reflection_model
                         *             https://www.tomdalling.com/blog/modern-opengl/07-more-lighting-ambient-specular-attenuation-gamma/
                         */
                        if (shading) {
                            
                            // part 1: ambient light = lightIntensity * k_ambient  (added directly later)
                            
                            // part 2: diffuse reflection
                            if (voxGrad.mag == 0) {
                                // no gradient case -> uniform medium -> no reflection (law of physics)
                                VectorMath.setVector(normVec, 0.0, 0.0, 0.0);
                                N_dot_L = 0.0;   
                            } 
                            else {  // normal case
                                // Bill:
                                // 1. Although we can do   N_dot_L = (gradient dot L) / ||gradient||,
                                //    (gradient / ||gradient||) is required for part 3
                                // 2. max(0, ) means: no refletion for light coming from the backside of the object surface
                                // 3. The minus sign: minus gradient goes outward (from strong intensity to weak)
                                VectorMath.setVector(normVec, voxGrad.x / voxGrad.mag, voxGrad.y / voxGrad.mag, voxGrad.z / voxGrad.mag);
                                N_dot_L = Math.max(0, -VectorMath.dotproduct(lightVec, normVec));
                            }
                            
                            // part 3: specular reflection
                            //     V = viewVec
                            //     R = (2 * N_dot_L) N - L, where L = lightVec
                            // Therefore 
                            //    V_dot_R = 2 * N_dot_L * V_dot_N - V_dot_L
                            V_dot_R = 2 * N_dot_L * VectorMath.dotproduct(viewVec, normVec) - VectorMath.dotproduct(viewVec, lightVec);
                            
                            // part 1 + 2 + 3
                            voxelColor.r = lightIntensity[0] * k_ambient + voxelColor.r * k_diff * N_dot_L + voxelColor.r * k_spec * Math.pow(V_dot_R, alpha);
                            voxelColor.g = lightIntensity[1] * k_ambient + voxelColor.g * k_diff * N_dot_L + voxelColor.g * k_spec * Math.pow(V_dot_R, alpha);
                            voxelColor.b = lightIntensity[2] * k_ambient + voxelColor.b * k_diff * N_dot_L + voxelColor.b * k_spec * Math.pow(V_dot_R, alpha);
                        
                        }  // if shading
                        
                        // Huilin: accumulate the final color components for this sample point "pixelCoord" (same as compositing)
                        finalColor.a = voxelColor.a + (1-voxelColor.a) * finalColor.a;
                        finalColor.r = voxelColor.r * voxelColor.a + (1 - voxelColor.a) * finalColor.r;
                        finalColor.g = voxelColor.g * voxelColor.a + (1 - voxelColor.a) * finalColor.g;
                        finalColor.b = voxelColor.b * voxelColor.a + (1 - voxelColor.a) * finalColor.b;
                    }  // if (val > 0)
                }  // for k
                
                // fill BufferedImage
                if (resolution > 1) {  // lower resolution
                    setImageARGBwithResolution(i, j, finalColor);
                } else {  // normal case
                    image.setRGB(i, j, finalColor.toARGB());
                }
            }  // i
        }  // j
    }  // transfer2d()
    
    /**
     * Bill: set image with lower resolution to speed up
     * @param i >= 2
     * @param j >= 2
     * @param color 
     */
    private void setImageARGBwithResolution(int i, int j, TFColor color) {
        
        // parse only once
        int argb = color.toARGB();
        // set rectangular range, truncate at border pixels
        int i_end = Math.min(image.getHeight(), i + resolution);
        int j_end = Math.min(image.getWidth(), j + resolution);
        // set pixels within the rectantular range
        for (int ii = i; ii < i_end; ii++) {   // i to i_end-1
            for (int jj = j; jj < j_end; jj++) {  // j to j_end-1
                image.setRGB(ii, jj, argb);
            }
        }
    }
    
    /**
     * Bill: refactored for clarity
     */
    private void clearImage() {
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }
    }
    
        
    private void drawBoundingBox(GL2 gl) {
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor4d(1.0, 1.0, 1.0, 1.0);
        gl.glLineWidth(1.5f);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glDisable(GL.GL_LINE_SMOOTH);
        gl.glDisable(GL.GL_BLEND);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPopAttrib();

    }

    @Override
    public void visualize(GL2 gl) {


        if (volume == null) {
            return;
        }

        drawBoundingBox(gl);

        gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, viewMatrix, 0);

        long startTime = System.currentTimeMillis();
        /**
         * The main rendering action happens here
         */        
        switch (renderingMode) {
        case 0:
            slicer(viewMatrix);
            break;
        case 1:
            mip(viewMatrix);
            break;
        case 2:
            compositing(viewMatrix);
            break;
        case 3:
            transfer2d(viewMatrix);
            break;
        }

       
        
        long endTime = System.currentTimeMillis();
        double runningTime = (endTime - startTime);
        panel.setSpeedLabel(Double.toString(runningTime));

        Texture texture = AWTTextureIO.newTexture(gl.getGLProfile(), image, false);

        gl.glPushAttrib(GL2.GL_LIGHTING_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        // draw rendered image as a billboard texture
        texture.enable(gl);
        texture.bind(gl);
        double halfWidth = image.getWidth() / 2.0;
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glTexCoord2d(0.0, 0.0);
        gl.glVertex3d(-halfWidth, -halfWidth, 0.0);
        gl.glTexCoord2d(0.0, 1.0);
        gl.glVertex3d(-halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 1.0);
        gl.glVertex3d(halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 0.0);
        gl.glVertex3d(halfWidth, -halfWidth, 0.0);
        gl.glEnd();
        texture.disable(gl);
        texture.destroy(gl);
        gl.glPopMatrix();

        gl.glPopAttrib();


        if (gl.glGetError() > 0) {
            System.out.println("some OpenGL error: " + gl.glGetError());
        }

    }
    private BufferedImage image;
    private double[] viewMatrix = new double[4 * 4];

    @Override
    public void changed() {
        for (int i=0; i < listeners.size(); i++) {
            listeners.get(i).changed();
        }
    }
}
